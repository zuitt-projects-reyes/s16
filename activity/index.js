/*
   ACTIVITY

>> Create a variable called number that will store the value of the number provided by the user via the prompt.

>> Create a for loop that will:
		 initialized with the number provided by the user, 
		 will stop when the value is less than or equal to 0 and 
		 will decrease by 1 every iteration.

>> Create a condition that if the current value is less than or equal to 50, stop the loop.

>> Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

>> Create another condition that if the current value is divisible by 5, print the number.

*/

let number = Number(prompt("Give me a number"));
console.log(`The number you provided is ${number}.`);

for (number; number >= 0; number--) {
	if (number <= 50) {
		console.log(`The current value is at ${number}. Terminating the loop.`);
		break;
	} else if (number % 10 === 0) {
		console.log('The number you divided is divisible by 10. Skipping the number.');
		continue;
	} else if (number % 5 === 0) {
		console.log(number);
	}
}